<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    protected $fillable = [
        'document_id',
        'product_id',
        'quantity',
        'price'
    ];
}
