<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = [
        'customer_id',
        'seller_id',
        'subtotal',
        'igv',
        'total',
    ];

    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

    public function seller()
    {
        return $this->belongsTo('App\Seller');
    }

    public function products()
    {
        return $this->belongsToMany('App\Product', 'details', 'document_id', 'product_id')
                    ->withPivot('quantity', 'price')
                    ->withTimestamps();
    }
}
