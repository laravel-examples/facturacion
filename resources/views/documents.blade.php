@extends('layout')

@section('content')
    <div class="container">
        @livewire('document-component')
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        window.livewire.on('closeModal', () => {
            $('.modal').modal('hide');
        });
    </script>
@endsection
